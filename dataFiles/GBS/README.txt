Raw input data at this location...

http://www.maizegenetics.net/tassel/GBSTestData.tar

# First 200,000 bp from Maize Chromosomes 9 and 10.
0		Chr9_10-200000/
674		Chr9_10-200000/KeepSpecifiedReadsinFastqPlugin_config.xml
12299		Chr9_10-200000/logfile
179890		Chr9_10-200000/C05F2ACXX_5_fastq.gz
134656		Chr9_10-200000/C08L7ACXX_6_fastq.gz

# First 20,000,000 bp from Maize Chromosomes 9 and 10.
0		Chr9_10-20000000/
678		Chr9_10-20000000/KeepSpecifiedReadsinFastqPlugin_config.xml
12305		Chr9_10-20000000/logfile
166051492	Chr9_10-20000000/C05F2ACXX_5_fastq.gz
155706587	Chr9_10-20000000/C08L7ACXX_6_fastq.gz

# Reference Maize Genome for first 20,000,000 bp of Chromosomes 9 and 10.
40012231	ZmB73_RefGen_v2_chr9_10_1st20MB.fasta

# Key File
23903		Pipeline_Testing_key.txt

# Directory where expected results are stored.
		ExpectedResults

# Temp directory where produced files are written
		tempDir

# Commands for running Tassel GBS Pipeline with 20MB test data.
run_pipeline.pl -Xmx5g -configFile 20MB_XMLs/FastqToTagCountPlugin.xml
run_pipeline.pl -Xmx5g -configFile 20MB_XMLs/MergeMultipleTagCountPlugin.xml
run_pipeline.pl -Xmx5g -configFile 20MB_XMLs/TagCountToFastqPlugin.xml
run bowtie2 - Results from this already in ExpectResults directory.
run_pipeline.pl -Xmx5g -configFile 20MB_XMLs/SAMConverterPlugin.xml
run_pipeline.pl -Xmx5g -configFile 20MB_XMLs/SeqToTBTHDF5Plugin.xml
run_pipeline.pl -Xmx5g -configFile 20MB_XMLs/ModifyTBTHDF5Plugin_P.xml
run_pipeline.pl -Xmx5g -configFile 20MB_XMLs/DiscoverySNPCallerPlugin.xml

# Commands for running Tassel GBS Pipeline with 200KB test data.
run_pipeline.pl -Xmx5g -configFile 200KB_XMLS/FastqToTagCountPlugin.xml
run_pipeline.pl -Xmx5g -configFile 200KB_XMLS/MergeMultipleTagCountPlugin.xml
run_pipeline.pl -Xmx5g -configFile 200KB_XMLS/TagCountToFastqPlugin.xml
run bowtie2 - Results from this already in ExpectResults directory.
run_pipeline.pl -Xmx5g -configFile 200KB_XMLS/SAMConverterPlugin.xml
run_pipeline.pl -Xmx5g -configFile 200KB_XMLS/SeqToTBTHDF5Plugin.xml
run_pipeline.pl -Xmx5g -configFile 200KB_XMLS/ModifyTBTHDF5Plugin_P.xml
run_pipeline.pl -Xmx5g -configFile 200KB_XMLS/DiscoverySNPCallerPlugin.xml

See this for details how this was created...

https://basecamp.com/2254511/projects/3042956-production-gbs/todos/49569565-create-test-input
