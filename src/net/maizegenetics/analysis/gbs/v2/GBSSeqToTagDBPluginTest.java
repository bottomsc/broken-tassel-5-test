/*
 * FastqToTagCountPluginTest
 */
package net.maizegenetics.analysis.gbs.v2;

import net.maizegenetics.constants.GBSConstants;
import net.maizegenetics.constants.GeneralConstants;
import net.maizegenetics.dna.map.*;
import net.maizegenetics.dna.tag.Tag;
import net.maizegenetics.dna.tag.TagData;
import net.maizegenetics.dna.tag.TagDataSQLite;
import net.maizegenetics.dna.tag.TaxaDistribution;
import net.maizegenetics.util.LoggingUtils;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Ed Buckler
 */
public class GBSSeqToTagDBPluginTest {
    private final static String outTagFasta=GBSConstants.GBS_TEMP_DIR + "tagsForAlign.fa.gz";
    private final static String inTagSAM=GBSConstants.GBS_TEMP_DIR + "tagsForAlign910auto.sam";

    public GBSSeqToTagDBPluginTest() {
    }

    /**
     * Test of performFunction method, of class FastqToTagCountPlugin.
     */
    @Test
    public void testGBSSeqToTagDBPlugin() {
        LoggingUtils.setupDebugLogging();
        System.out.println(Paths.get(GBSConstants.GBS_GBS2DB_FILE).toAbsolutePath().toString());
        try{
            Files.deleteIfExists(Paths.get(GBSConstants.GBS_GBS2DB_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        long time=System.nanoTime();
        System.out.println();

        new GBSSeqToTagDBPlugin()
                .enzyme("ApeKI")
                .inputDirectory(GBSConstants.GBS_INPUT_DIR)
                .outputDatabaseFile(GBSConstants.GBS_GBS2DB_FILE)
                .keyFile(GBSConstants.GBS_TESTING_KEY_FILE)
                .kmerLength(64)
                .minKmerCount(5)
                .minimumQualityScore(20)
                //.maximumMapMemoryInMb(5500)
                .performFunction(null);
        System.out.printf("TotalTime %g sec%n", (double) (System.nanoTime() - time) / 1e9);
        //String outName= GeneralConstants.TEMP_DIR+"TaxaTest.db";
        //TagsByTaxaHDF5Builder.create(, (Map<Tag,TaxaDistribution>)ds.getData(0).getData(),null);
    }

    @Test
    public void testTagExportPlugin() throws Exception {
        LoggingUtils.setupDebugLogging();
        System.out.println("Running testTagExportPlugin");
        new TagExportToFastqPlugin()
                .inputDB(GBSConstants.GBS_GBS2DB_FILE)
                .outputFile(outTagFasta)
                .performFunction(null);

    }

    @Test
    public void testSAMImportPlugin() throws Exception {
        LoggingUtils.setupDebugLogging();
        System.out.println("Running testTagExportPlugin");
        new SAMToGBSdbPlugin()
                .gBSDBFile(GBSConstants.GBS_GBS2DB_FILE)
                //.sAMInputFile(GBSConstants.GBS_EXPECTED_BOWTIE_SAM_FILE)
                .sAMInputFile(inTagSAM)
                .performFunction(null);
    }

    @Test
    public void GBSSeqToTagDBPluginAppendTest() throws Exception {
        // Testing duplicate tagTagIDMap when GBSSEq run twice in a row (run this test twice in a row!)
        System.out.println(Paths.get(GBSConstants.GBS_GBS2DB_FILE).toAbsolutePath().toString());
        try{
            Files.deleteIfExists(Paths.get(GBSConstants.GBS_GBS2DB_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        long time=System.nanoTime();
        System.out.println("Running GBSSeqToTagDBPluginAppendTest");

        new GBSSeqToTagDBPlugin()
        .enzyme("ApeKI")
        .inputDirectory(GBSConstants.GBS_INPUT_DIR)
        .outputDatabaseFile(GBSConstants.GBS_GBS2DB_FILE)
        .keyFile(GBSConstants.GBS_TESTING_KEY_FILE)
        .kmerLength(64)
        .minKmerCount(5)
        .minimumQualityScore(20)
        .performFunction(null);

        // Grab existing data from db, append to tagCntMap
        TagData tdw =new TagDataSQLite(GBSConstants.GBS_GBS2DB_FILE);
        Set<Tag> firstRunTags = tdw.getTags();
        Map<Tag, TaxaDistribution> firstTDM = tdw.getAllTagsTaxaMap(); 
        ((TagDataSQLite)tdw).close();

        BufferedWriter fileWriter = null;
        try { 
            fileWriter = new BufferedWriter(new FileWriter(GBSConstants.GBS_TEMP_DIR + "dbTagsFirstRun.txt"));
            String headers = "sequence\ttotalDepth\tTaxaDist_ToString";
            fileWriter.write(headers);
            for (Map.Entry<Tag, TaxaDistribution> entry : firstTDM.entrySet()) {
                fileWriter.write("\n");
                Tag tag = entry.getKey();
                TaxaDistribution td = entry.getValue();
                fileWriter.write(tag.sequence());
                fileWriter.write("\t");
                String totalDepth = "totalDepth=" + td.totalDepth(); // This prints depth correctly
                //fileWriter.write(td.totalDepth()); // gave weird output in file
                fileWriter.write(totalDepth);
                fileWriter.write("\t");
                fileWriter.write(td.toString());
            }
            fileWriter.close();
        }catch(IOException e) {
            System.out.println(e);
        }
        fileWriter.close();

        System.out.println("Calling GBSSeqToTagDBPlugin 2nd time to append data.");
        // Run again, this time append tags.  This uses the same fastq files, so
        // we expect the same tags, but with increased total depth at all kept positions
        new GBSSeqToTagDBPlugin()
        .enzyme("ApeKI")
        .inputDirectory(GBSConstants.GBS_INPUT_DIR)
        .outputDatabaseFile(GBSConstants.GBS_GBS2DB_FILE)
        .keyFile(GBSConstants.GBS_TESTING_KEY_FILE)
        .kmerLength(64)
        .minKmerCount(5)
        .minimumQualityScore(20)
        .performFunction(null);

        // Grab existing data from db, store in map
        tdw =new TagDataSQLite(GBSConstants.GBS_GBS2DB_FILE);
        Set<Tag> secondRunTags = tdw.getTags();
        Map<Tag, TaxaDistribution> secondTDM = tdw.getAllTagsTaxaMap(); 
        ((TagDataSQLite)tdw).close();  //todo autocloseable should do this but it is not working.

        // Verify number of tags are the same.  BEcause we are using the
        // same files it will have the same tags.  The tags/taxa are the same,
        // the depths at each tag should be greater after the second run that appends.
        assertEquals(firstRunTags.size(),secondRunTags.size());

        // Write files to the database so tags can be compared between files
        // and differences analysed.
        try { 
            fileWriter = new BufferedWriter(new FileWriter(GBSConstants.GBS_TEMP_DIR + "dbTagsAfterAppendRun.txt"));
            String headers = "sequence\ttotalDepth\tTaxaDist_ToString";
            fileWriter.write(headers);
            for (Map.Entry<Tag, TaxaDistribution> entry : secondTDM.entrySet()) {
                fileWriter.write("\n");
                Tag tag = entry.getKey();
                TaxaDistribution td = entry.getValue();
                fileWriter.write(tag.sequence());
                fileWriter.write("\t");
                String totalDepth = "totalDepth=" + td.totalDepth(); 
                fileWriter.write(totalDepth);
                fileWriter.write("\t");
                fileWriter.write(td.toString());
            }
            fileWriter.close();
        }catch(IOException e) {
            System.out.println(e);
        }
        fileWriter.close();

        // LCJ - I found "short" tags (ie tags with length < maxTagL size) never
        // increased their tag depths value.  Need to understand why this is.  The
        // values were always the same before and after append, the counts were
        // often greater than minTagCount so that wasn't the issue.

        //      Set<Tag> firstTags = firstTDM.keySet();
        //        for (Map.Entry<Tag, TaxaDistribution> secondEntry : secondTDM.entrySet()) {                    
        //            Tag secondTag = secondEntry.getKey();
        //            if (firstTags.contains(secondTag)) {
        //                System.out.printf("Assert Tag %s depths2 %d greater than depths1 %d\n",
        //                        secondTag.sequence(), secondEntry.getValue().totalDepth(),firstTDM.get(secondTag).totalDepth());
        //                assertTrue(secondEntry.getValue().totalDepth() > firstTDM.get(secondTag).totalDepth());
        //            }                    
        //        }
        System.out.println("Finished GBSSeqToTagDBPluginAppendTest.");
    }
    // full pipeline and biology evaluation helper methods moved
    // to EvaluateSNPCallQualityOfPipelineTest.java
}