package net.maizegenetics.analysis.gbs.v2;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;

import junit.framework.Assert;
import net.maizegenetics.constants.GBSConstants;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GeneralPosition;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.dna.snp.Allele;
import net.maizegenetics.dna.tag.Tag;
import net.maizegenetics.dna.tag.TagBuilder;
import net.maizegenetics.dna.tag.TaxaDistBuilder;
import net.maizegenetics.dna.tag.TaxaDistribution;
import net.maizegenetics.util.LoggingUtils;
import net.maizegenetics.util.Tuple;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class DiscoverySNPCallerPluginV2Test {
    private Map<Tag,Tuple<Boolean,TaxaDistribution>> tagTaxaDistMap;
    private Map<Tag, Tuple<Boolean, TaxaDistribution>> tagTaxaDistMap2;


    @Before
    public void setUp() throws Exception {
        LoggingUtils.setupLogging();
        List<String> sequences= ImmutableList.of("ACGACGACG","ACtACGACG","ACGACGtCG","ACtACGGtCG",
                "TGTCGTCGT");
        List<String> sequences2= ImmutableList.of("ACGACGACGTT","ACtACGACGTT","ACGACGtCGTT","AtACGGtCGAAGG",
                "TGTCGTCGT");
        List<Boolean> direction= ImmutableList.of(true, true, true, true,false);
        List<TaxaDistribution> taxaDists=ImmutableList.of(
                TaxaDistBuilder.create(10,new int[]{0,1,2,3},new int[]{1,2,1,3}),
                TaxaDistBuilder.create(10,new int[]{4,5},new int[]{1,1}),
                TaxaDistBuilder.create(10,new int[]{3,6,7},new int[]{3,2,1}),
                TaxaDistBuilder.create(10,8),
                TaxaDistBuilder.create(10,1)
        );
        List<Tag> tags= sequences.stream().map(s -> TagBuilder.instance(s).build()).collect(Collectors.toList());
        tags.set(0,TagBuilder.instance(tags.get(0).sequence()).reference().build());  //make first tag the reference tag
        tagTaxaDistMap=new HashMap<>();
        for (int i = 0; i < tags.size(); i++) {  //This should all be doable with a Stream zip function, but it was removed from beta versions, and I don't know how to do it now.
            tagTaxaDistMap.put(tags.get(i),new Tuple<>(direction.get(i),taxaDists.get(i)));
        }
        
        // Create second tagTaxaDistMap for testing filtered alignments.  new sequences, first one is
        // still the reference, same taxDist for maps 1 and 2
        List<Tag> tags2= sequences2.stream().map(s -> TagBuilder.instance(s).build()).collect(Collectors.toList());
        tags2.set(0,TagBuilder.instance(tags2.get(0).sequence()).reference().build());  //make first tag the reference tag
        tagTaxaDistMap2=new HashMap<>();
        for (int i = 0; i < tags2.size(); i++) {  //This should all be doable with a Stream zip function, but it was removed from beta versions, and I don't know how to do it now.
            tagTaxaDistMap2.put(tags2.get(i),new Tuple<>(direction.get(i),taxaDists.get(i)));
        }
    }

    @Test
    public void testAlignTags() throws Exception {
        Map<Tag,String> alignedTags= usePrivAlignTags(tagTaxaDistMap);
        alignedTags.forEach((t,s) -> System.out.println(s+ ": from "+t.toString()+ tagTaxaDistMap.get(t).toString()));
        alignedTags.forEach((t, s) -> assertEquals("Alignment Length incorrect", 10, s.length()));
        Position startPos=new GeneralPosition.Builder(new Chromosome("1"),40000).build();

        Table<Position, Byte, List<TagTaxaDistribution>> tAlign=usePrivConvertAlignmentToTagTable(alignedTags,
                tagTaxaDistMap,  startPos);

        Table<Position, Byte, List<TagTaxaDistribution>> alignT2= TreeBasedTable.create();
        tAlign.rowMap().forEach((p, mBTTD) -> {
            if(mBTTD.entrySet().size()>1) {
                mBTTD.forEach((a,ttdl) -> alignT2.put(p,a,ttdl));
            }
        } );
        alignT2.rowMap().forEach((p,ttd) -> System.out.println(p.getPosition()+" :"+ttd.entrySet().size()));
        System.out.println(tAlign.toString());
    }
    
	@Test
    public void testFilterAlignedTags() throws Exception {
		System.out.println("Running test TestFilterAlignedtags");
    	System.out.println("Begin sequence1 tests:\n");
        Map<Tag,String> alignedTagsUnfiltered= usePrivAlignTags(tagTaxaDistMap);
        alignedTagsUnfiltered.forEach((t,s) -> System.out.println(s+ ": from "+t.toString()+ tagTaxaDistMap.get(t).toString()));
        alignedTagsUnfiltered.forEach((t, s) -> assertEquals("Alignment Length incorrect", 10, s.length()));
        Position startPos=new GeneralPosition.Builder(new Chromosome("1"),40000).build();
        
        // The filtered tags and their alignment look as below.  note that all are below 0.5
        // ref tag:     ACGAC-GACG
        // aligned tag: ACGAC-GACG value: 0.0 for  IC=0,NC=9
        // aligned tag: ACTAC-GACG value: 0.0 for  IC=0,NC=9
        // aligned tag: ACTACGGTCG value: 0.1 for  IC=1,NC=9
        // aligned tag: ACGACG-TCG value: 0.2 for  IC=2,NC=8
        // aligned tag: ACGACG-ACA value: 0.2 for  IC=2,NC=8
        Map<Tag,String> alignedTags = usePrivFilterAlignTags(alignedTagsUnfiltered, startPos, 0.5);

        assertEquals(alignedTags.size(),5);
        System.out.println("\nsequence1: Tags successfully aligned within threshold range of 0.5");
        convertAlignedTagsToTable(alignedTags,tagTaxaDistMap, startPos);
        
        // Run again with filter  0.12
        // Same aligned tags as above, the last 2 cause the loci to be tossed
        alignedTags = usePrivFilterAlignTags(alignedTagsUnfiltered, startPos, 0.12); 
        assertEquals(alignedTags,null);
        System.out.println("\nsequence1: Null returned for threshold of 0.12 - tags at loci tossed\n");
        
    	// Repeat tests with second sequence of tags with 0.8 and 0.12 threshold 
        // The filtered tags and their alignments should look as below.
        // ref tag:     A------CGACGACGTT 
        // aligned tag: A------CGACGTCGTT value: 0.0    for IC=0, NC=11
        // aligned tag: A------CGACGACA-- value: 0.1818 for IC=2, NC=9
        // aligned tag: ATACGGTCGAAG--G-- value: 0.5882 for IC=10, NC=7
        // aligned tag: A------CGACGACGTT value: 0.0 for  IC=0,  NC=11
        // aligned tag: A------CTACGACGTT value: 0.0 for  IC=0, NC=11
        System.out.println("Begin sequence2 tests:\n");
        alignedTagsUnfiltered= usePrivAlignTags(tagTaxaDistMap2);
        alignedTagsUnfiltered.forEach((t,s) -> System.out.println(s+ ": from "+t.toString()+ tagTaxaDistMap2.get(t).toString()));
        alignedTagsUnfiltered.forEach((t, s) -> assertEquals("Alignment Length incorrect", 17, s.length()));
        
        //  Run with 0.8 as filter - all tags fall within range
        alignedTags = usePrivFilterAlignTags(alignedTagsUnfiltered, startPos, 0.8);
        assertEquals(alignedTags.size(),5);   // All tags pass threshold 
        System.out.println("\nsequence2: Tags successfully aligned within threshold range of 0.8\n");
        convertAlignedTagsToTable(alignedTags,tagTaxaDistMap2, startPos);
        
        //Run again with filter  0.12 - tags are tossed. the 0.1818 and 0.5882 above exceed the threshold
        alignedTags = usePrivFilterAlignTags(alignedTagsUnfiltered, startPos, 0.12);
        assertEquals(alignedTags,null); // 2 tags fall out, so null is returned.
        System.out.println("\nsequence2: Null returned for threshold of 0.12 - tags at loci tossed");
    }

    @Test
	public void testFullSNPCaller() throws Exception {
	    System.out.println("Running testFullSNPCaller");
	    Chromosome nine = new Chromosome("9");
	    Chromosome ten = new Chromosome("10");
	    new DiscoverySNPCallerPluginV2()
	            .inputDB(GBSConstants.GBS_GBS2DB_FILE)
	            .minMinorAlleleFreq(0.01)
	            .startChromosome(nine)
	            .endChromosome(ten)
	            .performFunction(null);
	}

    @Test
    public void testSinglePositionFiltering() throws Exception {
        System.out.println("DiscoverySNPCallerPluginV2Test.testSinglePositionFiltering");
        Position startPos=new GeneralPosition.Builder(new Chromosome("1"),10000).build();
        DiscoverySNPCallerPluginV2 caller=new DiscoverySNPCallerPluginV2();
        Multimap<Tag,Allele> alignT=caller.findAlleleByAlignment(startPos,tagTaxaDistMap,new Chromosome("1"));
        alignT.asMap().forEach((t, alleleCollection) -> {
                    System.out.print(t.toString());
                    alleleCollection.forEach(a -> System.out.print(a.position().getPosition() + "=" + a.allele() + ","));
                    System.out.println();
                }
        );
    }

    
    private void convertAlignedTagsToTable (Map<Tag,String> alignedTags, Map<Tag,Tuple<Boolean,TaxaDistribution>> distMap, Position startPos) throws Exception {
        Table<Position, Byte, List<TagTaxaDistribution>> tAlign=usePrivConvertAlignmentToTagTable(alignedTags,
                distMap,  startPos);
        Table<Position, Byte, List<TagTaxaDistribution>> alignT2= TreeBasedTable.create();
        tAlign.rowMap().forEach((p, mBTTD) -> {
            if(mBTTD.entrySet().size()>1) {
                mBTTD.forEach((a,ttdl) -> alignT2.put(p,a,ttdl));
            }
        } );
        alignT2.rowMap().forEach((p,ttd) -> System.out.println(p.getPosition()+" :"+ttd.entrySet().size()));
        System.out.println(tAlign.toString());
    }
    
    private Map<Tag,String> usePrivAlignTags(Map<Tag,Tuple<Boolean,TaxaDistribution>> tags) throws Exception {
        DiscoverySNPCallerPluginV2 discoverySNPCallerPluginV2 = new DiscoverySNPCallerPluginV2();
        Class theClass = discoverySNPCallerPluginV2.getClass();
        Method theMethod = theClass.getDeclaredMethod("alignTags", new Class[] {Map.class, int.class} );
        theMethod.setAccessible(true);
        return (Map<Tag,String>)theMethod.invoke(discoverySNPCallerPluginV2,(Object)tags, 64);
    }
    
    private Map<Tag,String> usePrivFilterAlignTags(Map<Tag,String> tags, Position refStartPosition, double threshold) throws Exception {
        DiscoverySNPCallerPluginV2 discoverySNPCallerPluginV2 = new DiscoverySNPCallerPluginV2().gapAlignmentThreshold(threshold);
        Class theClass = discoverySNPCallerPluginV2.getClass();
        Method theMethod = theClass.getDeclaredMethod("filterAlignedTags", new Class[] {Map.class, Position.class, double.class} );
        theMethod.setAccessible(true);
        return (Map<Tag,String>)theMethod.invoke(discoverySNPCallerPluginV2,(Object)tags, refStartPosition, threshold);
    }

    private Table<Position, Byte, List<TagTaxaDistribution>>  usePrivConvertAlignmentToTagTable(Map<Tag,String> alignedTags,
                              Map<Tag,Tuple<Boolean,TaxaDistribution>> tagTaxaDistMap, Position refStartPosition) throws Exception {
        DiscoverySNPCallerPluginV2 discoverySNPCallerPluginV2 = new DiscoverySNPCallerPluginV2();
        Class theClass = discoverySNPCallerPluginV2.getClass();
        Method theMethod = theClass.getDeclaredMethod("convertAlignmentToTagTable", new Class[] {Map.class, Map.class, Position.class} );
        theMethod.setAccessible(true);
        return (Table<Position, Byte, List<TagTaxaDistribution>>)theMethod.invoke(discoverySNPCallerPluginV2,
                (Object)alignedTags, (Object)tagTaxaDistMap,(Object)refStartPosition);
        //org.apache.commons.lang.reflect.MethodUtils - this could be used to simplify this.
    }
}