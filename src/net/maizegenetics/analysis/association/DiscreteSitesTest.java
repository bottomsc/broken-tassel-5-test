package net.maizegenetics.analysis.association;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import net.maizegenetics.constants.GeneralConstants;
import net.maizegenetics.constants.TutorialConstants;
import net.maizegenetics.dna.snp.ExportUtils;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.phenotype.GenotypePhenotype;
import net.maizegenetics.phenotype.GenotypePhenotypeBuilder;
import net.maizegenetics.phenotype.Phenotype;
import net.maizegenetics.phenotype.PhenotypeBuilder;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.util.LoggingUtils;
import net.maizegenetics.util.TableReport;
import net.maizegenetics.util.TableReportTestUtils;
import net.maizegenetics.util.TableReportUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DiscreteSitesTest {
	GenotypeTable myGenotype;
//	static {LoggingUtils.setupDebugLogging();}

	@Before
	public void setUp() throws Exception {
		String genotypeFilename = TutorialConstants.HAPMAP_CHR_9_10_FILENAME;
		myGenotype = ImportUtils.readFromHapmap(genotypeFilename);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReplicatedTaxa() {
		String filename = GeneralConstants.DATA_DIR + "CandidateTests/earht_with_rep.txt";
		Phenotype myPhenotype = new PhenotypeBuilder().fromFile(filename).build().get(0);
		GenotypePhenotype myGenoPheno = new GenotypePhenotypeBuilder().genotype(myGenotype).phenotype(myPhenotype).intersect().build();

		DiscreteSitesFELM dslm = new DiscreteSitesFELM(new Datum("name", myGenoPheno, "comment"), null);
		
		dslm.solve();
		
		TableReport siteReport = dslm.siteReport();
		String expectedResults = GeneralConstants.EXPECTED_RESULTS_DIR + "discreteSitesReplicatedTaxa.txt";

		//to save the results to use for validation, uncomment the following line
//		TableReportUtils.saveDelimitedTableReport(siteReport, "\t", new File(expectedResults));
		
		TableReport expectedSiteReport = TableReportUtils.readDelimitedTableReport(expectedResults, "\t");

		File tempFile; 
		try {
			tempFile = File.createTempFile("test", null);
			TableReportUtils.saveDelimitedTableReport(siteReport, "\t", tempFile);
			TableReport observedSiteReport = TableReportUtils.readDelimitedTableReport(tempFile.getPath(), "\t");
			TableReportTestUtils.compareTableReports(expectedSiteReport, observedSiteReport);
		} catch(IOException e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testUnreplicatedTaxa() {
		String filename = TutorialConstants.TRAITS_FILENAME;
		Phenotype myPhenotype = new PhenotypeBuilder().fromFile(filename).keepAttributes(new int[]{0,1}).build().get(0);
		GenotypePhenotype myGenoPheno = new GenotypePhenotypeBuilder().genotype(myGenotype).phenotype(myPhenotype).intersect().build();

		DiscreteSitesFELM dslm = new DiscreteSitesFELM(new Datum("name", myGenoPheno, "comment"), null);
		dslm.solve();
		TableReport siteReport = dslm.siteReport();
		String expectedResults = GeneralConstants.EXPECTED_RESULTS_DIR + "discreteSitesFELM.txt";

		//to save the results to use for validation, uncomment the following line
//		TableReportUtils.saveDelimitedTableReport(siteReport, "\t", new File(expectedResults));

		TableReport expectedSiteReport = TableReportUtils.readDelimitedTableReport(expectedResults, "\t");

		File tempFile; 
		try {
			tempFile = File.createTempFile("test", null);
			TableReportUtils.saveDelimitedTableReport(siteReport, "\t", tempFile);
			TableReport observedSiteReport = TableReportUtils.readDelimitedTableReport(tempFile.getPath(), "\t");
			TableReportTestUtils.compareTableReports(expectedSiteReport, observedSiteReport);
		} catch(IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
