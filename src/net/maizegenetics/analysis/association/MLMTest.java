package net.maizegenetics.analysis.association;

import net.maizegenetics.analysis.association.MLMPlugin.CompressionType;
import net.maizegenetics.analysis.distance.EndelmanDistanceMatrix;
import net.maizegenetics.analysis.filter.FilterSiteBuilderPlugin;
import net.maizegenetics.constants.TutorialConstants;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTableUtils;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.phenotype.GenotypePhenotype;
import net.maizegenetics.phenotype.GenotypePhenotypeBuilder;
import net.maizegenetics.phenotype.Phenotype;
import net.maizegenetics.phenotype.PhenotypeBuilder;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.taxa.distance.DistanceMatrix;
import net.maizegenetics.util.TableReport;
import net.maizegenetics.util.TableReportTestUtils;
import net.maizegenetics.util.TableReportUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MLMTest {
	private final static String statsNocompNop3d = "dataFiles/ExpectedResults/MLM_statistics_nocomp_nop3d.txt";
	private final static String effectsNocompNop3d = "dataFiles/ExpectedResults/MLM_effects_nocomp_nop3d.txt";
	private final static String residualsNocompNop3d = "dataFiles/ExpectedResults/MLM_residuals_nocomp_nop3d.txt";
	private final static String statsCompp3d = "dataFiles/ExpectedResults/MLM_statistics_comp_p3d.txt";
	private final static String effectsCompp3d = "dataFiles/ExpectedResults/MLM_effects_comp_p3d.txt";
	private final static String residualsCompp3d = "dataFiles/ExpectedResults/MLM_residuals_comp_p3d.txt";
	private final static String compressionCompp3d = "dataFiles/ExpectedResults/MLM_compression_comp_p3d.txt";
	GenotypeTable myGenotype;
	Phenotype myPhenotype;
	DistanceMatrix myK;
	DataSet myDataset;
	
	@Before
	public void setUp() throws Exception {
		GenotypeTable geno = ImportUtils.readFromHapmap(TutorialConstants.HAPMAP_CHR_9_10_FILENAME);
		myK = EndelmanDistanceMatrix.getInstance(geno, 6, null);
		geno = GenotypeTableUtils.removeSitesOutsideRange(geno, 0, 50);
		myGenotype = new FilterSiteBuilderPlugin().siteMinCount(100).siteMinAlleleFreq(0.1).runPlugin(geno);
		myPhenotype = new PhenotypeBuilder().fromFile(TutorialConstants.TRAITS_FILENAME).keepAttributes(new int[]{0,1}).build().get(0);
		GenotypePhenotype genopheno = new GenotypePhenotypeBuilder().genotype(myGenotype).phenotype(myPhenotype).intersect().build();
		myDataset = new DataSet(new Datum[]{
				new Datum("genotypePhenotype", genopheno, "no comment"),
				new Datum("kinship", myK, "no comment")
				}, null);
		
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testNoCompressionNoP3D() {
		MLMPlugin myPlugin = new MLMPlugin(null, false);
		myPlugin.setCompressionType(CompressionType.None);
		myPlugin.setUseP3D(false);
		DataSet resultSet = myPlugin.performFunction(myDataset);
		
		TableReport statReferenceReport = TableReportUtils.readDelimitedTableReport(statsNocompNop3d, "\t");
		TableReport effectReferenceReport = TableReportUtils.readDelimitedTableReport(effectsNocompNop3d, "\t");
		TableReport residualReferenceReport = TableReportUtils.readDelimitedTableReport(residualsNocompNop3d, "\t");
		TableReport statTestReport = (TableReport) resultSet.getData(1).getData();
		TableReport effectTestReport  = (TableReport) resultSet.getData(2).getData();
		TableReport residualTestReport  = (TableReport) resultSet.getData(0).getData();
		
		TableReportTestUtils.compareTableReports(statReferenceReport, statTestReport,.01);
		TableReportTestUtils.compareTableReports(effectReferenceReport, effectTestReport,.01);
		TableReportTestUtils.compareTableReports(residualReferenceReport, residualTestReport,.01);
	}
	
	@Test
	public void testCompressionP3D() {
		MLMPlugin myPlugin = new MLMPlugin(null, false);
		myPlugin.setCompressionType(CompressionType.Optimum);
		myPlugin.setUseP3D(true);
		DataSet resultSet = myPlugin.performFunction(myDataset);
		
		TableReport statReferenceReport = TableReportUtils.readDelimitedTableReport(statsCompp3d, "\t");
		TableReport effectReferenceReport = TableReportUtils.readDelimitedTableReport(effectsCompp3d, "\t");
		TableReport residualReferenceReport = TableReportUtils.readDelimitedTableReport(residualsCompp3d, "\t");
		TableReport compressionReferenceReport = TableReportUtils.readDelimitedTableReport(compressionCompp3d, "\t"); 
		TableReport statTestReport = (TableReport) resultSet.getData(1).getData();
		TableReport effectTestReport  = (TableReport) resultSet.getData(2).getData();
		TableReport residualTestReport  = (TableReport) resultSet.getData(0).getData();
		TableReport compressionTestReport  = (TableReport) resultSet.getData(3).getData();
		
		TableReportTestUtils.compareTableReports(statReferenceReport, statTestReport,.01);
		TableReportTestUtils.compareTableReports(effectReferenceReport, effectTestReport,.01);
		TableReportTestUtils.compareTableReports(residualReferenceReport, residualTestReport,.01);
		TableReportTestUtils.compareTableReports(compressionReferenceReport, compressionTestReport, .01);
	}

}
