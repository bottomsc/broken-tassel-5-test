/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.maizegenetics.analysis.association;

import java.util.ArrayList;
import org.junit.Test;
import net.maizegenetics.analysis.data.FileLoadPlugin;
import net.maizegenetics.analysis.data.FileLoadPlugin.TasselFileType;
import net.maizegenetics.analysis.distance.Kinship;
import net.maizegenetics.constants.TutorialConstants;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.phenotype.Phenotype;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.taxa.distance.DistanceMatrix;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author chd45
 */
public class GenomicSelectionPluginTest {
    
    @Test 
    public void test1(){
        FileLoadPlugin flp = new FileLoadPlugin(null,false);
        flp.setTheFileType(TasselFileType.Hapmap);
        flp.setOpenFiles(new String[]{TutorialConstants.HAPMAP_FILENAME});
        DataSet ds1 = flp.performFunction(null);
        GenotypeTable geno = (GenotypeTable) ds1.getData(0).getData();
        DistanceMatrix kin = Kinship.createKinship(geno, Kinship.KINSHIP_TYPE.Endelman, GenotypeTable.GENOTYPE_TABLE_COMPONENT.Genotype);
        DataSet dsKin = new DataSet(new Datum("name",kin,""), null); 
        
        flp.setTheFileType(TasselFileType.Phenotype);
        flp.setOpenFiles(new String[]{TutorialConstants.TRAITS_FILENAME});
        DataSet ds2 = flp.performFunction(null);
        Phenotype pheno = (Phenotype) ds2.getData(0).getData();
        
        ArrayList<DataSet> mergedInputs = new ArrayList();
        mergedInputs.add(dsKin);
        mergedInputs.add(ds2);
        
        DataSet mergedData = DataSet.getDataSet(mergedInputs, flp);
        
        GenomicSelectionPlugin pluginTest = new GenomicSelectionPlugin(null,false);
        pluginTest.kFolds(5);
        pluginTest.nIterations(2);
        DataSet outData = pluginTest.processData(mergedData);
        
        //fireDataSetReturned(outData);
        //return outData;
        System.out.println(outData);
        assertTrue(true);
    }
           
    
    public GenomicSelectionPluginTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
