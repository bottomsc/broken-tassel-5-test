/*
 *  FilterSiteBuilderPluginTest
 * 
 *  Created on Oct 8, 2015
 */
package net.maizegenetics.analysis.filter;

import net.maizegenetics.constants.GeneralConstants;
import net.maizegenetics.constants.TutorialConstants;
import net.maizegenetics.dna.map.PositionList;
import net.maizegenetics.dna.snp.AlignmentTestingUtils;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.dna.snp.io.JSONUtils;
import net.maizegenetics.plugindef.DataSet;
import org.junit.Test;

/**
 *
 * @author Terry Casstevens
 */
public class FilterSiteBuilderPluginTest {

    private static final String TEST_DIR = GeneralConstants.DATA_DIR + "FilterSiteBuilderPlugin/";

    public FilterSiteBuilderPluginTest() {
    }

    /**
     * Test of positionList method, of class FilterSiteBuilderPlugin.
     */
    @Test
    public void testFilterPositionList() {

        System.out.println("Testing Filtering Genotype Table by Position List...");

        String expectedResultsFilename = TEST_DIR + "GenotypeFilteredByPositionList.hmp.txt.gz";
        GenotypeTable expectedAlign = ImportUtils.readFromHapmap(expectedResultsFilename, null);

        DataSet input = ImportUtils.readDataSet(TutorialConstants.HAPMAP_FILENAME);

        PositionList positions = JSONUtils.importPositionListFromJSON(TEST_DIR + "position_list.json.gz");

        DataSet output = new FilterSiteBuilderPlugin(null, false)
                .positionList(positions)
                .performFunction(input);

        GenotypeTable result = (GenotypeTable) output.getDataOfType(GenotypeTable.class).get(0).getData();

        AlignmentTestingUtils.alignmentsEqual(expectedAlign, result);

    }

}
