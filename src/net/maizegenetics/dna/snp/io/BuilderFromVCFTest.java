package net.maizegenetics.dna.snp.io;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.junit.Ignore;
import org.junit.Test;

import net.maizegenetics.constants.GeneralConstants;
import net.maizegenetics.dna.snp.ExportUtils;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.io.BuilderFromVCF;

import org.apache.commons.io.FileUtils;

public class BuilderFromVCFTest {

	//String directoryName = "/Users/zrm22/Desktop/VCFTestData/VCF_files/TestFiles/";
	String directoryName = GeneralConstants.DATA_DIR+"CandidateTests/VCFFiles/";
	//TESTS TODO
	//-Verify that the information is the same during import/export
	//-Verify information is same during sorting
	//-Verify GenotypeTables are correctly generated
	
	//Currently Ignoring tests as the file paths will not work unless set up correctly.
	
	//Test to make sure it can load correct VCF files
	//SuccessCondition: BuilderFromVCF generates a non-null GenotypeTable
	@Test
	public void standardVCFTest() {
		String fileName = directoryName+"correctVCF1.vcf";
		BuilderFromVCF builder = BuilderFromVCF.getBuilder(fileName);
		GenotypeTable table = builder.buildAndSortInMemory();
		assertNotNull("Standard VCF Test Table should not be null",table);
	}

	//Test to make sure it can handle ./. as the GT field
	//SuccessCondition: BuilderFromVCF generates a non-null GenotypeTable
	@Test
	public void missingAlleleTest() {
		String fileName = directoryName+"missingAlleleVCF1.vcf";
		BuilderFromVCF builder = BuilderFromVCF.getBuilder(fileName);
		GenotypeTable table = builder.buildAndSortInMemory();
		assertNotNull("Standard VCF Test Table should not be null",table);
	}
	
	//Test to make sure it catches when a haploid is encountered
	//SuccessCondition: BuilderFromVCF throws an exception with an appropriate error message
	@Test
	public void tas_509Test() {
	    String fileName = directoryName+"tas_509VCF1.vcf";
	    BuilderFromVCF builder = BuilderFromVCF.getBuilder(fileName);
	    try{
		GenotypeTable table = builder.buildAndSortInMemory();
		fail("Should have thrown an exception at the string \".:.:.:.:.\"");
	    }catch(IllegalStateException e) {
		    assertEquals(e.getMessage(),"java.lang.IllegalStateException: Error Processing VCF block: Found haploid information for the element: .:.:.:.:..\nExpected a diploid entry.");
		}
	}
	
	//Test for Missing FORMAT Tag
	@Test
	public void formatTest() {
	    String fileName = directoryName+"MissingFormat.vcf";
	    BuilderFromVCF builder = BuilderFromVCF.getBuilder(fileName);
            try{
                GenotypeTable table = builder.buildAndSortInMemory();
                fail("Should have thrown an exception when FORMAT is hit");
            }catch(IllegalStateException e) {
                    assertEquals(e.getMessage(),"java.lang.IllegalStateException: Error Processing VCF Block: Missing FORMAT tag.");
            }
	}
	
	//Test for Incorrectly ordered FORMAT Tag
	@Test
	public void formatOrderedTag() {
	    String fileName = directoryName+"GTFieldIncorrect.vcf";
            BuilderFromVCF builder = BuilderFromVCF.getBuilder(fileName);
            try{
                GenotypeTable table = builder.buildAndSortInMemory();
                fail("Should have thrown an exception when FORMAT is hit and GT is not first");
            }catch(IllegalStateException e) {
                    assertEquals(e.getMessage(),"java.lang.IllegalStateException: Error Processing VCF Block: GT field is not in first position of FORMAT.");
            }
	}
	
	//Test to make sure it catches when a file is unsorted by position
	//SuccessCondition: BuilderFromVCF throws an IllegalStateException
	@Test
	public void unSortedTest() {
	    String fileName = directoryName+"unsortedVCF1.vcf";
	    BuilderFromVCF builder = BuilderFromVCF.getBuilder(fileName);
	    try{
	        GenotypeTable table = builder.build();
	        fail("Should have thrown an exception as the file is unsorted by position");
	    }catch(IllegalStateException e) {
	        assertEquals(e.getMessage(),"BuilderFromVCF: Ordering incorrect. VCF file must be ordered by position. Please first use SortGenotypeFilePlugin to correctly order the file.");
	    }
	}
	
	@Test
	public void spacedHeaderTest() {
	    String fileName = directoryName+"correctHeaderVCF1.vcf";
	    BuilderFromVCF builder = BuilderFromVCF.getBuilder(fileName);
	    GenotypeTable table = builder.buildAndSortInMemory();
	    assertEquals("Failed Header with Spaces Test",table.numberOfTaxa(),1);
	}
	
	@Test
	//Basically round-trips a VCF file with INFO Tags
	public void infoTagPerSiteTest() {
	    //Read in file with INFO Tags
	    String fileName = directoryName+"infoTagVCF1.vcf";
	    BuilderFromVCF builder = BuilderFromVCF.getBuilder(fileName);
	    //Build GenotypeTable
	    GenotypeTable table = builder.buildAndSortInMemory();
	    
	    //Export GenotypeTable to VCF
	    String outputFileName = GeneralConstants.TEMP_DIR+"infoTagVCFExport.vcf";
            ExportUtils.writeToVCF(table, outputFileName, true);
            
            //Read in Exported and compare to a Input VCF file
            try {
                boolean filesMatch = FileUtils.contentEquals(new File(fileName), new File(outputFileName));              
                assertTrue("Exported File does not match Original File.  INFO tags are not consistent",filesMatch);
                
                //Check to see if Exported file matches expected Result
                String outputFileNameCorrect = GeneralConstants.EXPECTED_RESULTS_DIR+"infoTagVCFExpected.vcf";
                filesMatch = FileUtils.contentEquals(new File(outputFileName), new File(outputFileNameCorrect));
                assertTrue("Exported File does not match Expected File. INFO tags are not consistent",filesMatch);
                
                //Could check to see if we remove info that it will not match, but this is trivial as it is the inverse of the previous test
            }
            catch(Exception e) {
                System.out.println(e);
            }
	}
}
