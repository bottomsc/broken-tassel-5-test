package net.maizegenetics.dna.snp.genotypecall;

import org.junit.Test;

/**
 * Defines xxxx
 *
 * @author Ed Buckler
 */
public class BasicGenotypeMergeRuleTest {
    @Test
    public void testMergeCalls() throws Exception {
        BasicGenotypeMergeRule m=new BasicGenotypeMergeRule(0.01);
        byte[] a=new byte[]{0,2,0,0,0,0};
        byte[] b=new byte[]{0,2,0,0,0,0};
        byte[] apb=m.mergeWithDepth(a,b);
        System.out.printf("%d %d %d %n",m.callBasedOnDepth(a),m.callBasedOnDepth(b),m.callBasedOnDepth(apb));
        a=new byte[]{4,0,0,4,0,0};
        b=new byte[]{4,0,0,4,0,0};
        apb=m.mergeWithDepth(a,b);
        System.out.printf("%d %d %d %n",m.callBasedOnDepth(a),m.callBasedOnDepth(b),m.callBasedOnDepth(apb));
        a=new byte[]{0, 4, 0, 4, 0, 0};
        b=new byte[]{0, 8, 0, 8, 0, 0};
        apb=m.mergeWithDepth(a,b);
        System.out.printf("%d %d %d %n",m.callBasedOnDepth(a),m.callBasedOnDepth(b),m.callBasedOnDepth(apb));
    }


}
