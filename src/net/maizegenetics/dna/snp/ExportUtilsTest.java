/*
 * ExportUtilsTest
 */
package net.maizegenetics.dna.snp;

import net.maizegenetics.constants.GeneralConstants;
import net.maizegenetics.constants.TutorialConstants;
import net.maizegenetics.dna.snp.io.BuilderFromHapMap;
import org.junit.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.fail;

/**
 *
 * @author terryc
 */
public class ExportUtilsTest {

    public static char DELIMIT_CHAR = '\t';
    public static String EXPORT_TEMP_DIR = GeneralConstants.TEMP_DIR + "ExportUtilsTest/";
    public static String HAPMAP_TEMP_FILENAME = "temp_hapmap.hmp.txt";

    public ExportUtilsTest() {
    }

    private static void clearTestFolder() {
        File f=new File(EXPORT_TEMP_DIR);
        System.out.println(f.getAbsolutePath());
        if(f.listFiles()!=null) {
            for (File file : f.listFiles()) {
                if(file.getName().contains("hmp.") || file.getName().contains(".h5")) {
                    file.delete();
                    System.out.println("Deleting:"+file.toString());
                }
            }
        }
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        File tempDir = new File(EXPORT_TEMP_DIR);
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of writeToHapmap method, of class ExportUtils.
     */
    @Test
    public void testWriteToHapmap() throws IOException {
        System.out.println("Testing Export to Hapmap...");
        String outputFile = EXPORT_TEMP_DIR + HAPMAP_TEMP_FILENAME;
        File input = new File(TutorialConstants.HAPMAP_FILENAME);
        System.out.println("   Input File: " + input.getCanonicalPath());
        if (!input.exists()) {
            fail("Input File: " + TutorialConstants.HAPMAP_FILENAME + " doesn't exist.");
        }
        GenotypeTable expResult = ImportUtils.readFromHapmap(TutorialConstants.HAPMAP_FILENAME, null);
        ExportUtils.writeToHapmap(expResult, false, outputFile, DELIMIT_CHAR, null);
        GenotypeTable result = ImportUtils.readFromHapmap(outputFile, null);

        AlignmentTestingUtils.alignmentsEqual(expResult, result);
    }

    @Test
    public void testHDF5WithDepth() throws Exception {
        clearTestFolder();
        GenotypeTable a=BuilderFromHapMap.getBuilder(TutorialConstants.HAPMAP_FILENAME).sortTaxa().build(); //need a sort taxa method to make the two approaches equal
        System.out.println("Testing roundtrip of HDF5 with Depth");
        GenotypeTable aWithDepth=AlignmentTestingUtils.createRandomDepthForGenotypeTable(a, 1);
        ExportUtils.writeGenotypeHDF5(aWithDepth,EXPORT_TEMP_DIR+"rtDepth.t5.h5",true);
        GenotypeTable rtWithDepth=ImportUtils.readGuessFormat(EXPORT_TEMP_DIR+"rtDepth.t5.h5");
        AlignmentTestingUtils.alignmentsEqual(a, rtWithDepth);

        for (int t=0; t<aWithDepth.numberOfTaxa(); t++) {
            for (int s=0; s<aWithDepth.numberOfSites(); s++) {
                int[] oD=aWithDepth.depthForAlleles(t,s);
                int[] x2D=rtWithDepth.depthForAlleles(t,s);
                boolean same=GenotypeTableUtils.isEqual(aWithDepth.genotype(t,s),rtWithDepth.genotype(t,s));
                Assert.assertTrue("Error"+aWithDepth.genotypeAsString(t,s)+" values"+Arrays.toString(oD),same);
                Assert.assertArrayEquals("Depth not  correct",oD,x2D);
            }
        }
    }
}