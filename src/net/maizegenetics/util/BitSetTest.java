/*
 * BitSetTest
 */
package net.maizegenetics.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author terry
 */
public class BitSetTest {

    private final int myNumBits = 5000;
    private int myNumBitsSet = 100;
    private final BitSet myBitSet;
    private final TreeSet<Integer> mySetBits = new TreeSet<Integer>();

    public BitSetTest() {
        myBitSet = new OpenBitSet(myNumBits);
        for (int i = 0; i < myNumBitsSet; i++) {
            int index = (int) Math.round(Math.random() * myNumBits);
            myBitSet.fastSet(index);
            mySetBits.add(index);
        }
        myNumBitsSet = mySetBits.size();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of capacity method, of class BitSet.
     */
    public void testCapacity() {
        System.out.println("Testing BitSet Capacity...");
        assertEquals("Capacity Mismatch: ", myNumBits, myBitSet.capacity());
    }

    /**
     * Test of size method, of class BitSet.
     */
    public void testSize() {
        System.out.println("Testing BitSet Size...");
        assertEquals("Size Mismatch: ", myNumBits, myBitSet.size());
    }

    /**
     * Test of isEmpty method, of class BitSet.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("Testing BitSet IsEmpty...");
        assertEquals("IsEmpty Mismatch: ", false, myBitSet.isEmpty());
    }

    /**
     * Test of getBits method, of class BitSet.
     */
    public void testGetBits_0args() {
        System.out.println("getBits");
        BitSet instance = new BitSetImpl();
        long[] expResult = null;
        long[] result = instance.getBits();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBits method, of class BitSet.
     */
    public void testGetBits_int_int() {
        System.out.println("getBits");
        int startWord = 0;
        int endWord = 0;
        BitSet instance = new BitSetImpl();
        long[] expResult = null;
        long[] result = instance.getBits(startWord, endWord);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBits method, of class BitSet.
     */
    public void testGetBits_int() {
        System.out.println("getBits");
        int index = 0;
        BitSet instance = new BitSetImpl();
        long expResult = 0L;
        long result = instance.getBits(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBits method, of class BitSet.
     */
    public void testSetBits() {
        System.out.println("setBits");
        long[] bits = null;
        BitSet instance = new BitSetImpl();
        instance.setBits(bits);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLong method, of class BitSet.
     */
    public void testSetLong() {
        System.out.println("setLong");
        int wordNum = 0;
        long bits = 0L;
        BitSet instance = new BitSetImpl();
        instance.setLong(wordNum, bits);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNumWords method, of class BitSet.
     */
    public void testGetNumWords() {
        System.out.println("Testing BitSet GetNumWords...");
        assertEquals("GetNumWords Mismatch: ", BitUtil.bits2words(myNumBits), myBitSet.getNumWords());
    }

    /**
     * Test of setNumWords method, of class BitSet.
     */
    public void testSetNumWords() {
        System.out.println("setNumWords");
        int nWords = 0;
        BitSet instance = new BitSetImpl();
        instance.setNumWords(nWords);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of get method, of class BitSet.
     */
    public void testGet_int() {
        System.out.println("get");
        int index = 0;
        BitSet instance = new BitSetImpl();
        boolean expResult = false;
        boolean result = instance.get(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fastGet method, of class BitSet.
     */
    public void testFastGet_int() {
        System.out.println("fastGet");
        int index = 0;
        BitSet instance = new BitSetImpl();
        boolean expResult = false;
        boolean result = instance.fastGet(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of get method, of class BitSet.
     */
    public void testGet_long() {
        System.out.println("get");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        boolean expResult = false;
        boolean result = instance.get(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fastGet method, of class BitSet.
     */
    public void testFastGet_long() {
        System.out.println("fastGet");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        boolean expResult = false;
        boolean result = instance.fastGet(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBit method, of class BitSet.
     */
    public void testGetBit() {
        System.out.println("getBit");
        int index = 0;
        BitSet instance = new BitSetImpl();
        int expResult = 0;
        int result = instance.getBit(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of set method, of class BitSet.
     */
    public void testSet_long() {
        System.out.println("set");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        instance.set(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fastSet method, of class BitSet.
     */
    public void testFastSet_int() {
        System.out.println("fastSet");
        int index = 0;
        BitSet instance = new BitSetImpl();
        instance.fastSet(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fastSet method, of class BitSet.
     */
    public void testFastSet_long() {
        System.out.println("fastSet");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        instance.fastSet(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of set method, of class BitSet.
     */
    public void testSet_long_long() {
        System.out.println("set");
        long startIndex = 0L;
        long endIndex = 0L;
        BitSet instance = new BitSetImpl();
        instance.set(startIndex, endIndex);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fastClear method, of class BitSet.
     */
    public void testFastClear_int() {
        System.out.println("fastClear");
        int index = 0;
        BitSet instance = new BitSetImpl();
        instance.fastClear(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fastClear method, of class BitSet.
     */
    public void testFastClear_long() {
        System.out.println("fastClear");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        instance.fastClear(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of clear method, of class BitSet.
     */
    public void testClear_long() {
        System.out.println("clear");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        instance.clear(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of clear method, of class BitSet.
     */
    public void testClear_int_int() {
        System.out.println("clear");
        int startIndex = 0;
        int endIndex = 0;
        BitSet instance = new BitSetImpl();
        instance.clear(startIndex, endIndex);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of clear method, of class BitSet.
     */
    public void testClear_long_long() {
        System.out.println("clear");
        long startIndex = 0L;
        long endIndex = 0L;
        BitSet instance = new BitSetImpl();
        instance.clear(startIndex, endIndex);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAndSet method, of class BitSet.
     */
    public void testGetAndSet_int() {
        System.out.println("getAndSet");
        int index = 0;
        BitSet instance = new BitSetImpl();
        boolean expResult = false;
        boolean result = instance.getAndSet(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAndSet method, of class BitSet.
     */
    public void testGetAndSet_long() {
        System.out.println("getAndSet");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        boolean expResult = false;
        boolean result = instance.getAndSet(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fastFlip method, of class BitSet.
     */
    public void testFastFlip_int() {
        System.out.println("fastFlip");
        int index = 0;
        BitSet instance = new BitSetImpl();
        instance.fastFlip(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fastFlip method, of class BitSet.
     */
    public void testFastFlip_long() {
        System.out.println("fastFlip");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        instance.fastFlip(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of flip method, of class BitSet.
     */
    public void testFlip_long() {
        System.out.println("flip");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        instance.flip(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of flipAndGet method, of class BitSet.
     */
    public void testFlipAndGet_int() {
        System.out.println("flipAndGet");
        int index = 0;
        BitSet instance = new BitSetImpl();
        boolean expResult = false;
        boolean result = instance.flipAndGet(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of flipAndGet method, of class BitSet.
     */
    public void testFlipAndGet_long() {
        System.out.println("flipAndGet");
        long index = 0L;
        BitSet instance = new BitSetImpl();
        boolean expResult = false;
        boolean result = instance.flipAndGet(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of flip method, of class BitSet.
     */
    @Test
    public void testFlip_long_long() {
        System.out.println("Testing BitSet Flip_long_long");
        int numBits = 489;
        BitSet instance = new OpenBitSet(numBits);
        Random random = new Random();
        List<Integer> listSetBits = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            int current = random.nextInt(100);
            listSetBits.add(current);
            instance.fastSet(current);
        }
        instance.flip(0, numBits);
        for (int i = 0; i < numBits; i++) {
            assertEquals("Flip Test: bit not flipped", instance.fastGet(i), !listSetBits.contains(i));
        }
    }

    /**
     * Test of cardinality method, of class BitSet.
     */
    public void testCardinality_0args() {
        System.out.println("cardinality");
        BitSet instance = new BitSetImpl();
        long expResult = 0L;
        long result = instance.cardinality();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of cardinality method, of class BitSet.
     */
    public void testCardinality_int() {
        System.out.println("cardinality");
        int index = 0;
        BitSet instance = new BitSetImpl();
        long expResult = 0L;
        long result = instance.cardinality(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of nextSetBit method, of class BitSet.
     */
    @Test
    public void testNextSetBit_int() {
        System.out.println("Testing BitSet NextSetBit_int...");
        Iterator<Integer> itr = mySetBits.iterator();
        int next = itr.next();
        for (int i = 0; i <= next; i++) {
            assertEquals(next, myBitSet.nextSetBit(i));
        }
        int previous = next;
        while (itr.hasNext()) {
            next = itr.next();
            for (int i = previous + 1; i <= next; i++) {
                assertEquals(next, myBitSet.nextSetBit(i));
            }
            previous = next;
        }
        for (int i = previous + 1; i < myNumBits; i++) {
            assertEquals(-1, myBitSet.nextSetBit(i));
        }
    }

    /**
     * Test of nextSetBit method, of class BitSet.
     */
    @Test
    public void testNextSetBit_long() {
        System.out.println("Testing BitSet NextSetBit_long...");
        Iterator<Integer> itr = mySetBits.iterator();
        long next = itr.next();
        for (long i = 0; i <= next; i++) {
            assertEquals(next, myBitSet.nextSetBit(i));
        }
        long previous = next;
        while (itr.hasNext()) {
            next = itr.next();
            for (long i = previous + 1; i <= next; i++) {
                assertEquals(next, myBitSet.nextSetBit(i));
            }
            previous = next;
        }
        for (long i = previous + 1; i < myNumBits; i++) {
            assertEquals(-1L, myBitSet.nextSetBit(i));
        }
    }

    /**
     * Test of previousSetBit method, of class BitSet.
     */
    @Test
    public void testPreviousSetBit_int() {
        System.out.println("Testing BitSet PreviousSetBit_int...");
        Iterator<Integer> itr = mySetBits.iterator();
        int previous = itr.next();
        for (int i = 0; i < previous; i++) {
            assertEquals(-1, myBitSet.previousSetBit(i));
        }
        while (itr.hasNext()) {
            int next = itr.next();
            for (int i = previous; i < next; i++) {
                assertEquals(previous, myBitSet.previousSetBit(i));
            }
            previous = next;
        }
        for (int i = previous; i < myNumBits; i++) {
            assertEquals(previous, myBitSet.previousSetBit(i));
        }
    }

    /**
     * Test of previousSetBit method, of class BitSet.
     */
    @Test
    public void testPreviousSetBit_long() {
        System.out.println("Testing BitSet PreviousSetBit_long...");
        Iterator<Integer> itr = mySetBits.iterator();
        long previous = itr.next();
        for (long i = 0; i < previous; i++) {
            assertEquals(-1L, myBitSet.previousSetBit(i));
        }
        while (itr.hasNext()) {
            long next = itr.next();
            for (long i = previous; i < next; i++) {
                assertEquals(previous, myBitSet.previousSetBit(i));
            }
            previous = next;
        }
        for (long i = previous; i < myNumBits; i++) {
            assertEquals(previous, myBitSet.previousSetBit(i));
        }
    }

    /**
     * Test of intersect method, of class BitSet.
     */
    public void testIntersect() {
        System.out.println("intersect");
        BitSet other = null;
        BitSet instance = new BitSetImpl();
        instance.intersect(other);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of union method, of class BitSet.
     */
    public void testUnion() {
        System.out.println("union");
        BitSet other = null;
        BitSet instance = new BitSetImpl();
        instance.union(other);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of remove method, of class BitSet.
     */
    public void testRemove() {
        System.out.println("remove");
        BitSet other = null;
        BitSet instance = new BitSetImpl();
        instance.remove(other);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of xor method, of class BitSet.
     */
    public void testXor() {
        System.out.println("xor");
        BitSet other = null;
        BitSet instance = new BitSetImpl();
        instance.xor(other);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of and method, of class BitSet.
     */
    public void testAnd() {
        System.out.println("and");
        BitSet other = null;
        BitSet instance = new BitSetImpl();
        instance.and(other);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of or method, of class BitSet.
     */
    public void testOr() {
        System.out.println("or");
        BitSet other = null;
        BitSet instance = new BitSetImpl();
        instance.or(other);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of andNot method, of class BitSet.
     */
    public void testAndNot() {
        System.out.println("andNot");
        BitSet other = null;
        BitSet instance = new BitSetImpl();
        instance.andNot(other);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of intersects method, of class BitSet.
     */
    public void testIntersects() {
        System.out.println("intersects");
        BitSet other = null;
        BitSet instance = new BitSetImpl();
        boolean expResult = false;
        boolean result = instance.intersects(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of ensureCapacityWords method, of class BitSet.
     */
    public void testEnsureCapacityWords() {
        System.out.println("ensureCapacityWords");
        int numWords = 0;
        BitSet instance = new BitSetImpl();
        instance.ensureCapacityWords(numWords);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of ensureCapacity method, of class BitSet.
     */
    public void testEnsureCapacity() {
        System.out.println("ensureCapacity");
        long numBits = 0L;
        BitSet instance = new BitSetImpl();
        instance.ensureCapacity(numBits);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of indexOfNthSetBit method, of class BitSet.
     */
    @Test
    public void testIndexOfNthSetBit() {
        System.out.println("Testing BitSet IndexOfNthSetBit...");
        assertEquals("IndexOfNthSetBit: ", -1, myBitSet.indexOfNthSetBit(0));
        Iterator<Integer> itr = mySetBits.iterator();
        int count = 1;
        while (itr.hasNext()) {
            int index = itr.next();
            assertEquals("IndexOfNthSetBit: ", index, myBitSet.indexOfNthSetBit(count));
            count++;
        }
        assertEquals("IndexOfNthSetBit: ", -1, myBitSet.indexOfNthSetBit(count));
    }

    /**
     * Test of getIndicesOfSetBits method, of class BitSet.
     */
    @Test
    public void testGetIndicesOfSetBits() {
        System.out.println("Testing BitSet GetIndicesOfSetBits...");
        int[] indices = myBitSet.getIndicesOfSetBits();
        assertEquals("Number bits set differ: ", myNumBitsSet, indices.length);
        for (int i = 0; i < indices.length; i++) {
            assertEquals("Indices differ: ", true, mySetBits.contains(indices[i]));
        }
    }

    public class BitSetImpl implements BitSet {

        public long capacity() {
            return 0L;
        }

        public long size() {
            return 0L;
        }

        public boolean isEmpty() {
            return false;
        }

        public long[] getBits() {
            return null;
        }

        public long[] getBits(int startWord, int endWord) {
            return null;
        }

        public long getBits(int index) {
            return 0L;
        }

        public void setBits(long[] bits) {
        }

        public void setLong(int wordNum, long bits) {
        }

        public int getNumWords() {
            return 0;
        }

        public void setNumWords(int nWords) {
        }

        public boolean get(int index) {
            return false;
        }

        public boolean fastGet(int index) {
            return false;
        }

        public boolean get(long index) {
            return false;
        }

        public boolean fastGet(long index) {
            return false;
        }

        public int getBit(int index) {
            return 0;
        }

        public void set(long index) {
        }

        public void fastSet(int index) {
        }

        public void fastSet(long index) {
        }

        public void set(long startIndex, long endIndex) {
        }

        public void fastClear(int index) {
        }

        public void fastClear(long index) {
        }

        public void clear(long index) {
        }

        public void clear(int startIndex, int endIndex) {
        }

        public void clear(long startIndex, long endIndex) {
        }

        public boolean getAndSet(int index) {
            return false;
        }

        public boolean getAndSet(long index) {
            return false;
        }

        public void fastFlip(int index) {
        }

        public void fastFlip(long index) {
        }

        public void flip(long index) {
        }

        public boolean flipAndGet(int index) {
            return false;
        }

        public boolean flipAndGet(long index) {
            return false;
        }

        public void flip(long startIndex, long endIndex) {
        }

        public long cardinality() {
            return 0L;
        }

        public long cardinality(int index) {
            return 0L;
        }

        public int nextSetBit(int index) {
            return 0;
        }

        public long nextSetBit(long index) {
            return 0L;
        }

        public int previousSetBit(int index) {
            return 0;
        }

        public long previousSetBit(long index) {
            return 0L;
        }

        public void intersect(BitSet other) {
        }

        public void union(BitSet other) {
        }

        public void remove(BitSet other) {
        }

        public void xor(BitSet other) {
        }

        public void and(BitSet other) {
        }

        public void or(BitSet other) {
        }

        public void andNot(BitSet other) {
        }

        public boolean intersects(BitSet other) {
            return false;
        }

        public void ensureCapacityWords(int numWords) {
        }

        public void ensureCapacity(long numBits) {
        }

        public void trimTrailingZeros() {
        }

        public int indexOfNthSetBit(int n) {
            return 0;
        }

        public int[] getIndicesOfSetBits() {
            return null;
        }
    }
}
