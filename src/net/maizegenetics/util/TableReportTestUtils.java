/*
 * TableReportTestUtils
 */
package net.maizegenetics.util;

import static org.junit.Assert.*;

/**
 *
 * @author Terry Casstevens
 */
public class TableReportTestUtils {

    private TableReportTestUtils() {
        // utility
    }

    public static void compareTableReports(TableReport report1, TableReport report2) {

        long numRows = report1.getRowCount();
        assertEquals("Mismatched Row Count: ", numRows, report2.getRowCount());

        int numColumns = report1.getColumnCount();
        assertEquals("Mismatched Column Count: ", numColumns, report2.getColumnCount());

        for (long r = 0; r < numRows; r++) {
            for (int c = 0; c < numColumns; c++) {
                assertEquals("Different Values in Distance Matrix: Row: " + r + " Column: " + c, report1.getValueAt(r, c).toString(), report2.getValueAt(r, c).toString());
            }
        }
    }
    
    /**
     * Use this function to compare TableReports that might hold values as Float or Double.
     * <p> 
     * @param report1	first TableReport
     * @param report2	second TableReport
     * @param delta		the maximum difference between two Double or Float values for which they will be considered equal
     */
    public static void compareTableReports(TableReport expected, TableReport observed, double delta) {
        long numRows = expected.getRowCount();
        assertEquals("Mismatched Row Count: ", numRows, observed.getRowCount());

        int numColumns = expected.getColumnCount();
        assertEquals("Mismatched Column Count: ", numColumns, observed.getColumnCount());

        for (long r = 0; r < numRows; r++) {
            for (int c = 0; c < numColumns; c++) {
            	Object cell1 = expected.getValueAt(r, c);
            	Object cell2 = observed.getValueAt(r, c);
            	
            	if (cell2 instanceof Double) {
            		double d1 = Double.parseDouble(expected.getValueAt(r,c).toString());
            		double d2 = (Double) observed.getValueAt(r,c);
            		assertEquals("Different Values in Distance Matrix: Row: " + r + " Column: " + c, d1, d2, delta);
            	} else if (cell2 instanceof Float) {
            		double d1 = Double.parseDouble(expected.getValueAt(r,c).toString());
            		double d2 = (Float) observed.getValueAt(r,c);
            		assertEquals("Different Values in Distance Matrix: Row: " + r + " Column: " + c, d1, d2, delta);
            	} else {
            		assertEquals("Different Values in Distance Matrix: Row: " + r + " Column: " + c, expected.getValueAt(r, c).toString(), observed.getValueAt(r, c).toString());
            	}
            }
        }
    }
    
    /**
     * Use this function to compare TableReports that might hold values as Float or Double.
     * <p> 
     * @param report1   first TableReport
     * @param report2   second TableReport
     * @param delta             the maximum difference between two Double or Float values for which they will be considered equal
     * Before comparing the two values, both are divided by the first value. 
     * This provides for appropriate comparison of values that are very different from 1, such as p-values.
     */
    public static void compareTableReportValues(TableReport expected, TableReport observed, double delta) {
        long numRows = expected.getRowCount();
        assertEquals("Mismatched Row Count: ", numRows, observed.getRowCount());

        int numColumns = expected.getColumnCount();
        assertEquals("Mismatched Column Count: ", numColumns, observed.getColumnCount());

        for (long r = 0; r < numRows; r++) {
            for (int c = 0; c < numColumns; c++) {
                Object cell1 = expected.getValueAt(r, c);
                Object cell2 = observed.getValueAt(r, c);
                
                if (cell2 instanceof Double) {
                        double d1 = Double.parseDouble(expected.getValueAt(r,c).toString());
                        double d2 = (Double) observed.getValueAt(r,c);
                        double d3 = d1 / d1;
                        double d4 = d2 / d1;
                        assertEquals("Different Values in Distance Matrix: Row: " + r + " Column: " + c, d3, d4, delta);
                } else if (cell2 instanceof Float) {
                        double d1 = Double.parseDouble(expected.getValueAt(r,c).toString());
                        double d2 = (Float) observed.getValueAt(r,c);
                        double d3 = d1 / d1;
                        double d4 = d2 / d1;
                        assertEquals("Different Values in Distance Matrix: Row: " + r + " Column: " + c, d3, d4, delta);
                } else {
                        assertEquals("Different Values in Distance Matrix: Row: " + r + " Column: " + c, expected.getValueAt(r, c).toString(), observed.getValueAt(r, c).toString());
                }
            }
        }
    }

}
