/*
 * CheckSumTest
 */
package net.maizegenetics.util;

import net.maizegenetics.constants.TutorialConstants;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Testing CheckSum with trivial test of hapmap file.
 * @author Dallas Kroon
 */
public class CheckSumTest {


    @Test
    public void testGetChecksum(){
        System.out.println("Testing CheckSum module");

        String protocol = "MD5";
        String knownMd5sum = "6309cda5e513b44cbbab8d3d9a77c364";

        String aChecksum = CheckSum.getChecksum(TutorialConstants.HAPMAP_FILENAME, protocol);

        assertEquals(knownMd5sum, aChecksum);

    }
}