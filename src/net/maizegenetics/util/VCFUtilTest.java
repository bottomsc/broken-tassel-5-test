package net.maizegenetics.util;

import org.junit.Test;

import java.util.Arrays;

/**
 * Unit test for the VCF quality scores
 *
 * @author Ed Buckler
 */
public class VCFUtilTest {
    @Test
    public void testGetScore() throws Exception {
        //currently both of these seem wrong, but I don't know what the answer is supposed to be.
        int[] missingScore=VCFUtil.getScore(0,0);
        System.out.println(Arrays.toString(missingScore));
        int[] lowScore=VCFUtil.getScore(1,1);
        System.out.println(Arrays.toString(missingScore));

    }
}
